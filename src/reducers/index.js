import { combineReducers } from 'redux';
import user from './user';
import goals from './goals';
import completeGoals from './completedGoals';

export default combineReducers({
  user,
  goals,
  completeGoals
});
