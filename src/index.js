import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Router, Route, Switch} from 'react-router-dom';
import { firebaseApp as firebase } from './firebase';
import { logUser } from './actions';
import reducer from './reducers';
import history from './history';

import App from './components/App';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';

const store = createStore(reducer);

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    // console.log('User – Signed In / Signed Up', user);
    const { email } = user;
    store.dispatch(logUser(email));
    history.push('/app');
  } else {
    // console.log('User - Sign Up Required / Signed Out', user);
    history.replace('/signin')
  }
});

const routes = (
  <Provider store={store}>
    <Router path="/" history={history}>
      <Switch>
        <Route path="/app" component={App} />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={SignUp} />
      </Switch>
    </Router>
  </Provider>
);

ReactDOM.render(routes, document.getElementById('root'));
