import React, { Component } from 'react';
import { completeGoalRef } from '../firebase';
import { connect } from 'react-redux';
import { setCompleted } from '../actions';

class CompleteGoalList extends Component {
  componentDidMount() {
    completeGoalRef.on('value', snap => {
      let completeGoals = [];
      snap.forEach(completeGoal => {
        const { email, title } = completeGoal.val();
        completeGoals.push({email, title});
      });
      this.props.setCompleted(completeGoals);
    });
  }

  clearCompleted() {
    completeGoalRef.set([]);
  }

  render() {
    console.log('this.props.completeGoals', this.props.completeGoals);
    return (
      <div>
        <ul>
          {
            this.props.completeGoals.map((goal, index) => {
              const { title, email } = goal;
              return (
                <li key={index}>
                  <strong>{title}</strong> completed by <em>{email}</em>
                </li>
              );
            })
          }
        </ul>
        <button
          className="btn btn-primary"
          onClick={() => this.clearCompleted()}>
          Clear All
        </button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { completeGoals } = state;
  return {
    completeGoals
  }
}

export default connect(mapStateToProps, { setCompleted })(CompleteGoalList);
