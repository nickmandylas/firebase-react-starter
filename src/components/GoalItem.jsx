import React, { Component } from 'react';
import { connect } from 'react-redux';
import { completeGoalRef, goalRef } from '../firebase';

class GoalItem extends Component {
  completeGoal() {
    const { email } = this.props.user;
    const { title, key } = this.props.goal;
    console.log('key', key);
    goalRef.child(key).remove();
    completeGoalRef.push({email, title});
  }

  render() {
    const { email, title } = this.props.goal;
    return (
      <li style={{margin  : '5px'}}>
        <strong>{title}</strong>
        <span> submitted by <em>{email}</em></span>
        <button
          className="btn btn-sm btn-primary"
          style={{marginLeft: '10px'}}
          onClick={() => this.completeGoal()}>
          Complete
        </button>
      </li>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state;
  return {
    user
  }
}

export default connect(mapStateToProps)(GoalItem);
