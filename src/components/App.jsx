import React, { Component } from 'react';
import { connect } from 'react-redux';
import {firebaseApp as firebase} from '../firebase';
import AddGoal from './AddGoal';
import GoalList from './GoalList';
import CompleteGoalList from './CompleteGoalList';

class App extends Component {

  signOut () {
    firebase.auth().signOut();
  }

  render() {
    return (
      <div style={{marginLeft: '10px'}}>
        <h3>Goal Admin</h3>
        <AddGoal />
        <hr />
        <h3>Due Goals</h3>
        <GoalList />
        <hr />
        <h3>Complete Goals</h3>
        <CompleteGoalList />
        <hr />
        <button
          className="btn btn-danger"
          onClick={() => this.signOut()}>
          Sign Out
        </button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  // console.log('state', state);
  return {}
}

export default connect(mapStateToProps)(App);
