import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { firebaseApp as firebase } from '../firebase';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: ''
    }
  }


  signIn() {
    const { email , password } = this.state;
    firebase.auth().signInWithEmailAndPassword(email, password).catch(
      error => {
        this.setState({error: error});
      }
    );
  }

  render() {
    return (
      <div className="form-inline" style={{margin: '2rem'}}>
        <h2>Sign In</h2>
        <div className="form-group">
          <input
            className="form-control"
            type="text"
            placeholder="email"
            onChange={event => this.setState({email: event.target.value})}
            style={{marginRight: '1rem'}}/>
          <input
            className="form-control"
            type="password"
            placeholder="password"
            onChange={event => this.setState({password: event.target.value})}  />
          <button
            className="btn btn-primary"
            type="button"
            onClick={() => this.signIn()}
            style={{marginLeft: '1rem'}}>Sign In
          </button>
        </div>
        <div
          style={{color: 'red', marginTop: '1rem'}}>
          {this.state['error'].message}
        </div>
        <div><Link to={'/signup'}>Haven't registered? Sign up!</Link></div>
      </div>
    );
  }
}

export default SignIn;
