import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAJ2_M20MCjzRdozfj6pCKPYRbDHG1Y1BE",
    authDomain: "fir-react-9b711.firebaseapp.com",
    databaseURL: "https://fir-react-9b711.firebaseio.com",
    projectId: "fir-react-9b711",
    storageBucket: "fir-react-9b711.appspot.com",
    messagingSenderId: "510170162963"
  };

export const firebaseApp = firebase.initializeApp(config);
export const goalRef = firebase.database().ref('goals');
export const completeGoalRef = firebase.database().ref('completeGoals');
